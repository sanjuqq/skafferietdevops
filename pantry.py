from file_manager import File_Manager

class Råvaror:
    def __init__(self, name, amount, unit):
        self.name = name
        self.amount = amount
        self.unit = unit

    def __str__(self):
        return f"{self.name}: {self.amount} {self.unit}"


class Pantry:
  
    def __init__(self):
        self.data = File_Manager("my_ingredients.json")
        self.pantry = []
        self.read_file()
        self.always_existing = ["Salt", "Pepper","Cooking oil","Olive oil"]
                    

    def read_file(self):
        file = self.data.read_file()
        for name, amount in file.items():
            self.add_new_ingredient(name, amount[0], amount[1])


    def add_new_ingredient(self, name, amount, unit):
        new_item = Råvaror(name, amount, unit)
        self.pantry.append(new_item)

    def is_ingredient_enough(self,item):
     
        ingredient = next((ingredient for ingredient in self.pantry if ingredient.name == item.name), None)
        if item.name in self.always_existing or (ingredient and ingredient.amount >= item.amount):
            return True

    def save_object_to_file(self):
        file = {}
        for item in self.pantry:
            file[item.name] = [item.amount, item.unit]
        self.data.write_file(file)    

