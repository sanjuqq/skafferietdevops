from file_manager import File_Manager
from pantry import Råvaror



class Recipe():
    def __init__(self, dish_name, instructions, portions, råvaror):
        self.dish_name = dish_name
        self.instructions = instructions
        self.portions = portions
        self.ingredients = råvaror

    def __str__(self):
        return(f'name: {self.dish_name} instr: {self.instructions}  portions: {self.portions} ingredient_amount: {len(self.ingredients)}')
       
class Cook_Book:
    def __init__(self):
        self.data = File_Manager('recept.json')
        self.recipe_data = []
        self.read_recipes()


    def read_recipes(self):

        '''Read from json file and creates object of recipes'''
        new_recipe_ingredients = []
        file_data = self.data.read_file()
        for key, value in file_data.items():
            new_recipe_name = key
            new_recipe_instructions = value['instructions']
            new_recipe_portions = value['portions']
            ingredient_names = value['ingredients'].keys()

            for name in ingredient_names:
                new_ingredient_name = name
                new_ingredient_value = value['ingredients'][name][0]
                new_ingredient_unit = value['ingredients'][name][1]

                new_ingredient = Råvaror(new_ingredient_name,
                                            new_ingredient_value,
                                            new_ingredient_unit)
                new_recipe_ingredients.append(new_ingredient)

            new_recipe = Recipe(new_recipe_name,
                                new_recipe_instructions,
                                new_recipe_portions,
                                new_recipe_ingredients)
            self.recipe_data.append(new_recipe)
            new_recipe_ingredients = []

    def new_recipe(self, dish_name, portions, instructions, ingredients):
        '''Create new recipe'''
        recipe = Recipe(dish_name, portions, instructions, ingredients)
        self.recipe_data.append(recipe)

    def save_recipe(self):
        '''Writes to file from memory in right json format'''
        data_to_file  = {}
        for recipe in self.recipe_data:
            ingredient_dict = {}
            for ingredients in recipe.ingredients:
                ingredient_dict.update({ingredients.name: [ingredients.amount, ingredients.unit]})
            data_to_file[recipe.dish_name] = {"ingredients":ingredient_dict,
                                                "portions":recipe.portions,
                                                "instructions":recipe.instructions}
            self.data.write_file(data_to_file)


