from recept import Cook_Book, Recipe
from pantry import Pantry, Råvaror



class Menu:
    def __init__(self):
        self.pantry = Pantry()
        self.cook_book = Cook_Book()
        self.menu()
        self.dish_list = []

    def menu(self):
        print("\nSkafferiet, \nPlease choose one of the following \n 1. Show Ingredients \n 2. Add Ingredients \n 3. Show Recipe\
            \n 4. Add Recipe \n 5. What should i eat today? \n Input save to save files")
        while True:
            choice = input("Your choice: ")
            if choice == "1":
                self.show_pantry()
            if choice == "2":
                 self.add_new_ingredients()
            if choice == "3":
                self.show_recipe()
            if choice == "4":
                self.add_recipe()
            if choice == "5":
                self.what_to_eat()
            if choice == 'save':
                self.save()  



    def show_pantry(self):
        for item in self.pantry.pantry:
            print(f"{item.name} {item.amount} {item.unit}")  


    def add_new_ingredients(self):
        name = input("What do you want to put in the pantry?: ") \
                .strip().capitalize()

        amount = float(input("How many/How munch in 'kg' or 'st': "))
        unit = input("Kg or st: ").capitalize()
        self.pantry.add_new_ingredient(name, amount, unit)
        print(f'New ingredient {name} is added to the pantry! ')

    def show_recipe(self):
        for item in self.cook_book.recipe_data:
            print(item)

    def add_recipe(self):
        ingredients_list = []
        name = input('Dish name: ')
        portion = input('Portions: ')
        instructions = input('Instructions for the dish: ')
        more_ingredients = True
        while more_ingredients:
            item = input("Enter ingredients: ").capitalize()
            amount = float(input("Enter amount: "))
            unit = input("Enter unit: ").capitalize()
            ingredients = Råvaror(item, amount, unit)
            ingredients_list.append(ingredients)
            more_ingredients = input('More ingredients?: ')
            if more_ingredients == 'no':
                break
            else:
                continue
        recipe = Recipe(name, instructions, portion, ingredients_list)
        self.cook_book.recipe_data.append(recipe)    

    def save(self):
           
        self.pantry.save_object_to_file()
        self.cook_book.save_recipe()
        print('Saved!')

    def what_to_eat(self):
        self.dish_list = []
        for recipe in self.cook_book.recipe_data:
            enough_ingredients = False
            for recipe_ingredient in recipe.ingredients:
                if not self.pantry.is_ingredient_enough(recipe_ingredient):
                    enough_ingredients = True
            if not enough_ingredients:
                self.dish_list.append(recipe.dish_name)
                
        def print_dish():       
            for dish in self.dish_list:
                print(f"We got enough resources for {dish}")

        print_dish()
        

Menu()