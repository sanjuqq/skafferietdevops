import json

class File_Manager:
    def __init__(self, file_path):
        self.file = file_path

    def read_file(self):
        try:
            with open(self.file, "r", encoding="utf-8") as file:
                return json.load(file)
        except FileNotFoundError:
            with open(self.file, "w", encoding="utf-8") as file:
                return {}

    def write_file(self, json_format):
        with open(self.file, "w", encoding="utf-8") as file:
            json.dump(json_format, file, indent=4, ensure_ascii=False)